package com.seminario.sabelotodo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SabelotodoApplication

fun main(args: Array<String>) {
	runApplication<SabelotodoApplication>(*args)
}
