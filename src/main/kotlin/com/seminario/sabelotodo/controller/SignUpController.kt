package com.seminario.sabelotodo.controller

import com.seminario.sabelotodo.model.Usuario
import com.seminario.sabelotodo.repositorio.UsuarioRepositorio
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/sign-up")
class SignUpController(val usuarioRepositorio: UsuarioRepositorio, val bCryptPasswordEncoder: BCryptPasswordEncoder) {

    @PostMapping
    fun signUp(@RequestBody usuario: Usuario) {

        usuario.password = bCryptPasswordEncoder.encode(usuario.password);
        usuarioRepositorio.save(usuario);
    }
}