package com.seminario.sabelotodo.controller

import com.seminario.sabelotodo.dto.Greeting
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicLong

@RestController
class GreetingController {

    val counter = AtomicLong()

    @GetMapping("/greeting")
    fun greeting(@RequestParam(value="name", defaultValue = "Wold") name: String) =
            Greeting(counter.incrementAndGet(), "Hello, $name.")
}