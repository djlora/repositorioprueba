package com.seminario.sabelotodo.model

import javax.persistence.*

@Entity
@Table(name = "usuario")
class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0;

    @Column(name = "username", length = 50, unique = true, nullable = false)
    var username: String = "";

    @Column(name = "password", length = 500, nullable = false)
    var password: String = "";

    @Column(name = "nombres", length = 50, nullable = false)
    var nombres: String = "";

    @Column(name = "apellidos", length = 50, nullable = false)
    var apellidos: String = "";
}